package com.example.appcopeeks.Controlleur;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.appcopeeks.Api.BGXpressService;

public  class CommandModel extends AppCompatActivity{

    private String changeNameCommand = "set sy d n ";
    private String changeMdpCommand = "set bl e k ";
    private String changeBaudCommand = "set ua b ";
    private String activePasswordCommand = "set bl e b 0 ";
    private String clearDeviceList = "clrb";
    private String changeGPIO = "gfu";

    public String getClearTabCommand(){
        String maCommand;
        maCommand = clearDeviceList+"\r\n";
        return maCommand;
    }
    public String getChangeNameCommand(String msgText){
       String maCommand;
       maCommand = changeNameCommand+msgText+"\r\n";
       return maCommand;
    }

    public String getSetGPIOCommand(int numberGPIO , String functionGPIO){
        String maCommand;
        maCommand = changeGPIO+" "+numberGPIO+" "+functionGPIO+"\r\n";
        return maCommand;
    }

    public String getActivePassword(){
        String maCommand;
        maCommand = activePasswordCommand+"\r\n";
        return maCommand;
    }
    public String getChangeMdpCommand(String msgText){
        String maCommand;
        maCommand = changeMdpCommand+msgText+"\r\n";
        return maCommand;
    }

    public String getChangeBaudCommand(String msgText){
        String maCommand;
        maCommand = changeBaudCommand+msgText+"\r\n";
        return maCommand;
    }
}
