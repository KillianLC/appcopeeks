package com.example.appcopeeks.Controlleur;


public class Capteur {
    private String id;
    private String type;
    private String unity;
    private String input_format;
    private String out_put;
    private String full_name;
    private String icon;
    private String max;
    private String min;
    private String format;

    public Capteur(String id, String type, String unity, String input_format, String out_put, String full_name, String icon, String max, String min, String format) {
        this.id = id;
        this.type = type;
        this.unity = unity;
        this.input_format = input_format;
        this.out_put = out_put;
        this.full_name = full_name;
        this.icon = icon;
        this.max = max;
        this.min = min;
        this.format = format;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUnity() {
        return unity;
    }

    public void setUnity(String unity) {
        this.unity = unity;
    }

    public String getInput_format() {
        return input_format;
    }

    public void setInput_format(String input_format) {
        this.input_format = input_format;
    }

    public String getOut_put() {
        return out_put;
    }

    public void setOut_put(String out_put) {
        this.out_put = out_put;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getMax() {
        return max;
    }

    public void setMax(String max) {
        this.max = max;
    }

    public String getMin() {
        return min;
    }

    public void setMin(String min) {
        this.min = min;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    @Override
    public String toString() {
        return "Capteur{" +
                "id='" + id + '\'' +
                ", type='" + type + '\'' +
                ", unity='" + unity + '\'' +
                ", input_format='" + input_format + '\'' +
                ", out_put='" + out_put + '\'' +
                ", full_name='" + full_name + '\'' +
                ", icon='" + icon + '\'' +
                ", max='" + max + '\'' +
                ", min='" + min + '\'' +
                ", format='" + format + '\'' +
                '}';
    }
}
