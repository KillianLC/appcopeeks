package com.example.appcopeeks.Controlleur;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.example.appcopeeks.Api.BGXpressService;

import java.util.ArrayList;
//This class is not used
public abstract class  Bluetooth extends AppCompatActivity {

    private String deviceName;
    private String deviceUUID;
    private String deviceRSSI;

    public BluetoothAdapter mBluetoothAdapter;
    public BluetoothGatt mBluetoothGatt;
    public BluetoothDevice mBluetoothDevice;
    public BGXpressService mBGXpressService;

    public ArrayList<BluetoothDevice> mBTDevices = new ArrayList<>();

    public Bluetooth(String deviceName, String deviceUUID, String deviceRSSI){
        this.deviceName = deviceName;
        this.deviceUUID = deviceUUID;
        this.deviceRSSI = deviceRSSI;
    }

    //accesseurs (Get) et les mutateurs (Set)
    public String getDeviceName(){
        return deviceName;
    }
    public void setDeviceName(String deviceName){
        this.deviceName = deviceName;
    }

    public String getDeviceUUID(){
        return deviceUUID;
    }
    public void setDeviceUUID(String deviceUUID){
        this.deviceUUID = deviceUUID;
    }

    public String getDeviceRSSI(){
        return deviceRSSI;
    }
    public void setDeviceRSSI(String deviceName){
        this.deviceRSSI = deviceRSSI;
    }



    //methodes
    protected void scan(){
        if(mBluetoothAdapter.isDiscovering()){
            IntentFilter discoverDevicesIntent = new IntentFilter(BluetoothDevice.ACTION_FOUND);
            registerReceiver(mBroadcastReceiver, discoverDevicesIntent);
        }
        else{
            IntentFilter discoverDevicesIntent = new IntentFilter(BluetoothDevice.ACTION_FOUND);
            registerReceiver(mBroadcastReceiver, discoverDevicesIntent);
        }
    }

    protected void connect(Context context, Intent intent){
        mBGXpressService.startActionStartScan(getBaseContext());
    }

    protected boolean isConnected(String address){
        boolean mBoolean = false;
        if(mBluetoothAdapter != null && address.equals(deviceUUID) && mBluetoothGatt != null){
            if(mBluetoothGatt.connect()){
                showToast("CONNECTED");
                 mBoolean = true;
            }else{
                mBoolean = false;
            }
        }
        return mBoolean;
    }

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver(){
        @Override
        public void onReceive(Context context, Intent intent) {
           getDevice(context,intent);
        }
    };

    private void showToast(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    private void getDevice(Context context, Intent intent){
        final String action = intent.getAction();
        showToast("OnReceive: Action Found");

        if (action.equals(BluetoothDevice.ACTION_FOUND)) {
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            mBTDevices.add(device);
            showToast( "onReceive: " + device.getName() + ": " + device.getAddress());
        }
    }
}
