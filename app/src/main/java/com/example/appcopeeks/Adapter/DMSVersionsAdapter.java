
package com.example.appcopeeks.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.util.Log;

import com.example.appcopeeks.R;
import com.example.appcopeeks.Vue.SelectionChangedListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class DMSVersionsAdapter extends RecyclerView.Adapter<DMSVersionsAdapter.ViewHolder> {

    private Context context;
    private LayoutInflater mInflater;
    private JSONArray mDataset;

    private View mSelectedRowView = null;
    private SelectionChangedListener mSelectionChangedListener;



    public DMSVersionsAdapter(Context context, SelectionChangedListener selectionChangedListener, JSONArray versions) {
        this.mSelectionChangedListener = selectionChangedListener;
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        mDataset = versions;

        if (null != mSelectedRowView) {
            mSelectedRowView.setBackgroundColor(0xFFFAFAFA);
            mSelectedRowView = null;
            mSelectionChangedListener.selectionDidChange(-1, null);
        }

    }

    @Override
    public DMSVersionsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.dms_version_list_row, parent, false);
        return new DMSVersionsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DMSVersionsAdapter.ViewHolder holder, int position) {

        try {
            JSONObject deviceRecord = mDataset.getJSONObject(position);

            String versionNumber = deviceRecord.getString("version");
            String versionTag = deviceRecord.getString("tag");
            int versionSize = deviceRecord.getInt("size");

            holder.getVersionNumberTextView().setText(versionNumber);
            holder.getVersionTagTextView().setText(versionTag);

            holder.getVersionBytesTextView().setText( String.format("%d bytes", versionSize) );


        } catch (JSONException e) {
            Log.e("bgx_dbg", "JSONException caught in DMSVersionsAdapter onBindViewHolder");
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        int count = 0;
        if (null != mDataset) {
            count = mDataset.length();
        }
        return count;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView versionNumberTextView;
        TextView versionTagTextView;
        TextView versionBytesTextView;

        ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            versionNumberTextView = itemView.findViewById(R.id.versionNumberTextView);
            versionTagTextView = itemView.findViewById(R.id.versionTagTextView);
            versionBytesTextView = itemView.findViewById(R.id.versionBytesTextView);
        }
        @Override
        public void onClick(View v) {


            if (null != mSelectedRowView) {
                mSelectedRowView.setBackgroundColor(0xFFFAFAFA);
                mSelectedRowView = null;
            }

            v.setBackgroundColor(Color.LTGRAY);
            mSelectedRowView = v;

            try {
                mSelectionChangedListener.selectionDidChange(getAdapterPosition(), mDataset.getJSONObject(getAdapterPosition()));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        public TextView getVersionNumberTextView() {
            return versionNumberTextView;
        }

        public TextView getVersionTagTextView() {
            return versionTagTextView;
        }

        public TextView getVersionBytesTextView() {
            return versionBytesTextView;
        }
    }

}
