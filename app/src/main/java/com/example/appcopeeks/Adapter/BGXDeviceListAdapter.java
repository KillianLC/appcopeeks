package com.example.appcopeeks.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.appcopeeks.Api.BGXpressService;
import com.example.appcopeeks.Vue.IndeterminateProgressActivity;
import com.example.appcopeeks.R;

import java.util.ArrayList;
import java.util.HashMap;

public class BGXDeviceListAdapter extends RecyclerView.Adapter<BGXDeviceListAdapter.ViewHolder> {

    private Context context;
    private ArrayList<HashMap<String, String>> mDataset;
    private LayoutInflater mInflater;

    private View mSelectedRowView = null;
    private Handler mHandler = new Handler();


    public BGXDeviceListAdapter(Context context, ArrayList dataset) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        this.mDataset = dataset;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.customlayout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final int rowPosition = position;
        final HashMap<String, String> deviceRecord = mDataset.get(position);
        String deviceName = deviceRecord.get("name");
        final String deviceAddress = deviceRecord.get("uuid");
        String rssiValueStr = deviceRecord.get("rssi");

        holder.getTextView().setText(deviceName);
        holder.getUuidTextView().setText(deviceAddress);
        holder.getRssiValueTextView().setText(rssiValueStr);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView myTextView;
        TextView uuidTextView;
        TextView rssiValueTextView;

        ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            myTextView = itemView.findViewById(R.id.tv_name);
            uuidTextView = itemView.findViewById(R.id.tv_uuid);
            rssiValueTextView = itemView.findViewById(R.id.tv_rssi);
        }

        @Override
        public void onClick(View v) {

            int position = getAdapterPosition();

            if (null != mSelectedRowView) {
                mSelectedRowView.setBackgroundColor(0x9AB89C);
                mSelectedRowView = null;
            }

            v.setBackgroundColor(Color.LTGRAY);
            mSelectedRowView = v;

            Log.d("bgx_dbg", "Adapter Position: " + position);

            HashMap<String, String> deviceRecord = mDataset.get(position);
            Log.d("bgx_dbg", "Selected Device: " + deviceRecord.get("name"));


            String deviceAddress = deviceRecord.get("uuid");

            BGXpressService.clearCancelFlag();


            BGXpressService.startActionBGXConnect(context, deviceAddress);

            Intent intent2 = new Intent(context, IndeterminateProgressActivity.class);
            intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            context.startActivity(intent2);

            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (null != mSelectedRowView) {
                        mSelectedRowView.setBackgroundColor(0x9AB89C);
                        mSelectedRowView = null;
                    }
                }
            }, 300);

        }

        public TextView getTextView() {
            return myTextView;
        }

        public TextView getUuidTextView() {
            return uuidTextView;
        }

        public TextView getRssiValueTextView() {
            return rssiValueTextView;
        }
    }
}

