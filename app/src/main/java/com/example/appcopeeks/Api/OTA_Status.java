package com.example.appcopeeks.Api;

/**
 * This is the OTA status that can be used to
 * drive a user interface as part of the
 * OTA_STATUS_MESSAGE.
 */
public enum OTA_Status {
    Invalid         /**< should never see this. It indicates an error has occured */
    ,Idle           /**< no OTA is happening */
    ,Downloading    /**< the firmware image is being downloaded through DMS */
    ,Installing     /**< the firmware image is being sent to the BGX device */
    ,Finishing      /**< the firmware image has been written and the bgx is being commanded to load it */
    ,Finished       /**< the BGX has acknowledged the command to load the firmware. The BGX is being rebooted */
    ,UserCanceled   /**< the intent ACTION_OTA_CANCEL has been received and the OTA operation is being canceled */
}
