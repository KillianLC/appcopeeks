package com.example.appcopeeks.Vue;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.appcopeeks.Api.BGXpressService;
import com.example.appcopeeks.Controlleur.CommandModel;
import com.example.appcopeeks.R;

import java.io.File;
import java.util.concurrent.TimeUnit;

import static com.example.appcopeeks.Api.BGXpressService.ACTION_OTA_WITH_IMAGE;

public class SettingActivity extends AppCompatActivity{

    private EditText edtName;
    private EditText edtMdp;
    private EditText edtBaud;
    private Button btnBaud;
    private Button btnUpdateName;
    private Button btnSave;
    private Button btnSetMdp;
    private Button btnCleanTab;
    private Button btnGpio;
    private TextView tvDateUTC;
    private CommandModel commandModel;
    private CheckBox cbId;
    private EditText edNbGPIO;
    private Spinner spFunction;
    private ImageButton imgBtnMAJ;
    private Button btnMAJ;
    private Context mContext;
    private TextView tvPath;

    protected static final int PICKFILE_RESULT_CODE = 140;
    private String FilePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_view);
        cbId = findViewById(R.id.cbId);
        commandModel = new CommandModel();
        btnBaud = findViewById(R.id.btnBaud);
        btnCleanTab = findViewById(R.id.btnCleanTab);
        edtName = findViewById(R.id.edtName);
        edtMdp = findViewById(R.id.edtMdp);
        edtBaud = findViewById(R.id.edtBaud);
        btnSetMdp = findViewById(R.id.btnSetMdp);
        btnUpdateName = findViewById(R.id.btnUpdateName);
        btnGpio = findViewById(R.id.btnGPIO);
        btnSave = findViewById(R.id.btnSave);
        tvDateUTC = findViewById(R.id.tvDateUTC);
        spFunction = findViewById(R.id.spFunction);
        edNbGPIO = findViewById(R.id.edNbGpio);
        imgBtnMAJ = findViewById(R.id.imgBtnMAJ);
        btnMAJ = findViewById(R.id.btnMAJ);
        tvPath = findViewById(R.id.tvPath);

        ArrayAdapter<String>fcAdapter = new ArrayAdapter<>(SettingActivity.this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.function));
        fcAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spFunction.setAdapter(fcAdapter);

        //UNIX time
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                TextView tdate = findViewById(R.id.tvDateUTC);
                                tdate.setText(getString(R.string.unixDate) + ": " + getDate());
                            }
                        });
                    }
                } catch (InterruptedException e) {

                }
            }
        };
        t.start();

        //allows to change the mode of sending CommandMode at the creation of the activity
        Intent intent = new Intent(BGXpressService.ACTION_WRITE_BUS_MODE);
        intent.setClass(this, BGXpressService.class);
        intent.putExtra("busmode", 3);
        startService(intent);

        //send an empty order
        Intent writeIntent = new Intent(BGXpressService.ACTION_WRITE_SERIAL_DATA);
        writeIntent.putExtra("value", "\r\n");
        writeIntent.setClass(getBaseContext(), BGXpressService.class);
        Log.d("INITIALISATION: ", "send an empty order!");
        startService(writeIntent);
    }

    protected void onDestroy() {
        super.onDestroy();

        Intent intent = new Intent(BGXpressService.ACTION_WRITE_BUS_MODE);
        intent.setClass(this, BGXpressService.class);
        intent.putExtra("busmode", 1);
        startService(intent);
        finish();
    }


    protected void onResume() {
        super.onResume();

        btnMAJ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    try {
                        Intent updateIntent = new Intent(mContext, BGXpressService.class);
                        updateIntent.setAction(ACTION_OTA_WITH_IMAGE);
                        updateIntent.putExtra("image_path", FilePath);
                        startService(updateIntent);
                    } catch (Exception e) {
                        showToast("MAJ failed");
                    }
            }
        });

        imgBtnMAJ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("file/*");
                startActivityForResult(intent,PICKFILE_RESULT_CODE);

            }
        });

        cbId.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    Intent writeIntent = new Intent(BGXpressService.ACTION_WRITE_SERIAL_DATA);
                    writeIntent.putExtra("value", commandModel.getActivePassword());
                    writeIntent.setClass(getBaseContext(), BGXpressService.class);
                    startService(writeIntent);
                    showToast("Activation mot de passe");
                }else{
                    Intent writeIntent = new Intent(BGXpressService.ACTION_WRITE_SERIAL_DATA);
                    writeIntent.putExtra("value", "set bl e b 1");
                    writeIntent.setClass(getBaseContext(), BGXpressService.class);
                    startService(writeIntent);
                    showToast("Désactivation mot de passe");
                }
            }
        });

        btnCleanTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Deletes all bonding information from previously paired devices.
                Intent writeIntent = new Intent(BGXpressService.ACTION_WRITE_SERIAL_DATA);
                writeIntent.putExtra("value", commandModel.getClearTabCommand());
                writeIntent.setClass(getBaseContext(), BGXpressService.class);
                startService(writeIntent);
                showToast("Liste des appareils supprimé");
            }
        });
        btnSetMdp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String edMdp = edtMdp.getText().toString();
                if(edMdp.equals("")){
                    edMdp = "none";
                }
                Intent writeIntent = new Intent(BGXpressService.ACTION_WRITE_SERIAL_DATA);
                writeIntent.putExtra("value", commandModel.getChangeMdpCommand(edMdp));
                writeIntent.setClass(getBaseContext(), BGXpressService.class);
                Log.d("TRANSMITTER: ", edMdp);
                startService(writeIntent);
                showToast(getString(R.string.mdpChange));
            }
        });

        btnBaud.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String edBaud = edtBaud.getText().toString();

                Intent writeIntent = new Intent(BGXpressService.ACTION_WRITE_SERIAL_DATA);
                writeIntent.putExtra("value", commandModel.getChangeBaudCommand(edBaud));
                writeIntent.setClass(getBaseContext(), BGXpressService.class);
                Log.d("TRANSMITTER: ", edBaud);
                startService(writeIntent);
                showToast(getString(R.string.baudModif)+edBaud);
            }
        });

        btnGpio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fcText = spFunction.getSelectedItem().toString();
                try {
                    int nbGpio = Integer.parseInt(edNbGPIO.getText().toString());
                    Intent writeIntent = new Intent(BGXpressService.ACTION_WRITE_SERIAL_DATA);
                    writeIntent.putExtra("value", commandModel.getSetGPIOCommand(nbGpio, fcText));
                    writeIntent.setClass(getBaseContext(), BGXpressService.class);
                    startService(writeIntent);
                    Log.d("TRANSMITTER: ", commandModel.getSetGPIOCommand(nbGpio, fcText));
                    showToast(commandModel.getSetGPIOCommand(nbGpio, fcText));
                }catch (Exception e){
                   showToast("Veillez indiquer un GPIO");
                }
            }
        });

        btnUpdateName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String edName = edtName.getText().toString();

                Intent writeIntent = new Intent(BGXpressService.ACTION_WRITE_SERIAL_DATA);
                writeIntent.putExtra("value", commandModel.getChangeNameCommand(edName));
                writeIntent.setClass(getBaseContext(), BGXpressService.class);
                Log.d("TRANSMITTER: ", edName);
                startService(writeIntent);
                showToast(getString(R.string.nameModif)+edName);
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateSave();
                showToast(getString(R.string.saveModif));
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode){
            case PICKFILE_RESULT_CODE: {
                if (resultCode == RESULT_OK) {
                    String FilePath = data.getData().getPath();
                    showToast(FilePath);
                    Log.i(getString(R.string.path), FilePath);
                    tvPath.setText(getString(R.string.path)+FilePath);
                }
            }
            break;
        }
    }

    private void updateSave() {
        try {
            Intent writeIntentSave = new Intent(BGXpressService.ACTION_WRITE_SERIAL_DATA);
            writeIntentSave.putExtra("value", "save");
            writeIntentSave.setClass(getBaseContext(), BGXpressService.class);
            Log.d("TRANSMITTER: ", "save" + "\r\n");
            startService(writeIntentSave);
        }
        catch (Exception e){
            showToast("Update failed");
        }
    }

    private String getDate() {
        String ts = String.valueOf(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()));
        return ts;
    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
