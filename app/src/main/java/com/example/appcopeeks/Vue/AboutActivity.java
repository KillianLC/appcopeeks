package com.example.appcopeeks.Vue;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.example.appcopeeks.R;


public class AboutActivity extends AppCompatActivity {
    private TextView tvAmp;
    private TextView tvLink;
    private TextView tvFeuille;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_view);
        tvAmp = findViewById(R.id.tvAmp);
        tvLink = findViewById(R.id.tvLink);
        tvFeuille = findViewById(R.id.tvFeuille);

        tvAmp.setText(getString(R.string.introBio));
        tvLink.setText(getString(R.string.storyCopeeks));
        tvFeuille.setText(getString(R.string.copeeksMission));
    }

    public void facebookLink(View view) {
        String url = "https://www.facebook.com/copeeks/";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    public void twitterLink(View view) {
        String url = "https://twitter.com/copeeks_sas";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    public void linkedinLink(View view) {
        String url = "https://www.linkedin.com/company/copeeks-sas";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    public void newsLink(View view) {
        String url = "http://eepurl.com/coBnM5";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }
}
