package com.example.appcopeeks.Vue;


import org.json.JSONObject;

public interface SelectionChangedListener {
    void selectionDidChange(int position, JSONObject selectedObject);
}
