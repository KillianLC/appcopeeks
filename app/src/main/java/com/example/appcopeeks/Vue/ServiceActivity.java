package com.example.appcopeeks.Vue;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.appcopeeks.Api.BGX_CONNECTION_STATUS;
import com.example.appcopeeks.Api.BGXpressService;
import com.example.appcopeeks.R;

public class ServiceActivity extends AppCompatActivity {
    private Button btnConsole;
    private Button btnSettings;
    private Button btnCapteur;
    private Button btnUpdate;
    public final Context mContext = this;
    private BroadcastReceiver mConnectionBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.service);
        btnConsole = findViewById(R.id.btnConsole);
        btnSettings = findViewById(R.id.btnSettings);
        btnCapteur = findViewById(R.id.btnCapteur);
        //btnUpdate = findViewById(R.id.btnUpdate);

        final IntentFilter bgxpressServiceFilter = new IntentFilter(BGXpressService.BGX_CONNECTION_STATUS_CHANGE);
        bgxpressServiceFilter.addAction(BGXpressService.BGX_MODE_STATE_CHANGE);
        bgxpressServiceFilter.addAction(BGXpressService.BGX_DATA_RECEIVED);
        bgxpressServiceFilter.addAction(BGXpressService.BGX_DEVICE_INFO);

        mConnectionBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (intent.getAction()) {
                    case BGXpressService.BGX_CONNECTION_STATUS_CHANGE: {
                        Log.d("bgx_dbg", "BGX Connection State Change");

                        BGX_CONNECTION_STATUS connectionState = (BGX_CONNECTION_STATUS) intent.getSerializableExtra("bgx-connection-status");
                        switch (connectionState) {
                            case CONNECTED:
                                Log.d("bgx_dbg", "DeviceDetails - connection state changed to CONNECTED");
                                break;
                            case CONNECTING:
                                Log.d("bgx_dbg", "DeviceDetails - connection state changed to CONNECTING");
                                break;
                            case DISCONNECTING:
                                Log.d("bgx_dbg", "DeviceDetails - connection state changed to DISCONNECTING");
                                break;
                            case DISCONNECTED:
                                //return to the activity_main if you are not connected
                                Log.d("bgx_dbg", "DeviceDetails - connection state changed to DISCONNECTED");
                                finish();
                                break;
                            case INTERROGATING:
                                Log.d("bgx_dbg", "DeviceDetails - connection state changed to INTERROGATING");
                                break;
                            default:
                                Log.d("bgx_dbg", "DeviceDetails - connection state changed to Unknown connection state.");
                                break;
                        }
                    }
                    break;
                }
            }
        };
        registerReceiver(mConnectionBroadcastReceiver, bgxpressServiceFilter);
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(mConnectionBroadcastReceiver);
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        btnConsole.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ServiceActivity.this, WriteReadSendActivity.class);
                startActivity(intent);
            }
        });
        btnCapteur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ServiceActivity.this, CapteurActivity.class);
                startActivity(intent);
            }
        });
        btnSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ServiceActivity.this, SettingActivity.class);
                startActivity(intent);
            }
        });
        /*btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ServiceActivity.this, FirmwareUpdate.class);
                startActivity(intent);
            }
        });*/
    }

    @Override
    public void onBackPressed() {
        Log.d("bgx_dbg", "Back button pressed.");
        disconnect();
        super.onBackPressed();
    }

    void disconnect() {
        Intent intent = new Intent(BGXpressService.ACTION_BGX_DISCONNECT);
        intent.setClass(mContext, BGXpressService.class);
        startService(intent);
    }

    public void aboutPage(View view) {
        Intent intent = new Intent(ServiceActivity.this, AboutActivity.class);
        startActivity(intent);
    }
}
