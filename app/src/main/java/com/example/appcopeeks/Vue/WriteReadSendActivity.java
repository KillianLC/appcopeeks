package com.example.appcopeeks.Vue;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.example.appcopeeks.Api.BGX_CONNECTION_STATUS;
import com.example.appcopeeks.Api.BGXpressService;
import com.example.appcopeeks.Controlleur.CommandModel;
import com.example.appcopeeks.R;

public class WriteReadSendActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {
    private EditText edSend;
    private Button btnSend;
    private Button btnClear;
    private Switch swType;
    private EditText edConsole;
    private CommandModel commandModel;
    private BroadcastReceiver mConnectionBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.write_read_page);
        edSend = findViewById(R.id.edSend);
        btnClear = findViewById(R.id.btnClear);
        btnSend = findViewById(R.id.btnSend);
        swType = findViewById(R.id.swType);
        edConsole = findViewById(R.id.tvConsole);
        commandModel = new CommandModel();

        final IntentFilter bgxpressServiceFilter = new IntentFilter(BGXpressService.BGX_CONNECTION_STATUS_CHANGE);
        bgxpressServiceFilter.addAction(BGXpressService.BGX_MODE_STATE_CHANGE);
        bgxpressServiceFilter.addAction(BGXpressService.BGX_DATA_RECEIVED);

        //allows to change the mode of sending StreamMode at the creation of the activity
        Intent intent = new Intent(BGXpressService.ACTION_WRITE_BUS_MODE);
        intent.setClass(this, BGXpressService.class);
        intent.putExtra("busmode", 1);
        startService(intent);

        showToast("Stream Mode");

        mConnectionBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (intent.getAction()) {
                    //writes the data received in the EditText
                    case BGXpressService.BGX_DATA_RECEIVED: {
                        String stringReceived = intent.getStringExtra("data");
                        edConsole.append(stringReceived);
                        Log.i("RECEIVER", stringReceived);
                    }
                    break;
                    case BGXpressService.BGX_MODE_STATE_CHANGE: {
                        Log.d("bgx_dbg", "BGX Bus Mode Change");
                    }
                    break;
                    case BGXpressService.BGX_CONNECTION_STATUS_CHANGE: {
                        Log.d("bgx_dbg", "BGX Connection State Change");

                        BGX_CONNECTION_STATUS connectionState = (BGX_CONNECTION_STATUS) intent.getSerializableExtra("bgx-connection-status");
                        switch (connectionState) {
                            case CONNECTED:
                                Log.d("bgx_dbg", "DeviceDetails - connection state changed to CONNECTED");
                                break;
                            case CONNECTING:
                                Log.d("bgx_dbg", "DeviceDetails - connection state changed to CONNECTING");
                                break;
                            case DISCONNECTING:
                                Log.d("bgx_dbg", "DeviceDetails - connection state changed to DISCONNECTING");
                                break;
                            case DISCONNECTED:
                                //return to the activity_main if you are not connected
                                Log.d("bgx_dbg", "DeviceDetails - connection state changed to DISCONNECTED");
                                finish();
                                break;
                            case INTERROGATING:
                                Log.d("bgx_dbg", "DeviceDetails - connection state changed to INTERROGATING");
                                break;
                            default:
                                Log.d("bgx_dbg", "DeviceDetails - connection state changed to Unknown connection state.");
                                break;
                        }
                    }
                    break;
                }
            }
        };

        registerReceiver(mConnectionBroadcastReceiver, bgxpressServiceFilter);
        swType.setOnCheckedChangeListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("bgx_dbg", "clear");
                edConsole.setText("");
            }
        });


        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent writeIntent = new Intent(BGXpressService.ACTION_WRITE_SERIAL_DATA);
                writeIntent.putExtra("value", edSend.getText().toString() + "\r\n");
                writeIntent.setClass(getBaseContext(), BGXpressService.class);
                Log.d("TRANSMITTER: ", edSend.getText().toString());
                edConsole.append("\n" + edSend.getText().toString() + " ");
                startService(writeIntent);
                edSend.setText("");
            }
        });
    }

    @Override
    public void onBackPressed() {
        Log.d("bgx_dbg", "Back button pressed. Tab list clear");
        super.onBackPressed();

        Intent writeIntent = new Intent(BGXpressService.ACTION_WRITE_SERIAL_DATA);
        writeIntent.putExtra("value", commandModel.getClearTabCommand());
        writeIntent.setClass(getBaseContext(), BGXpressService.class);
        startService(writeIntent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("bgx_dbg", "Unregistering the connectionBroadcastReceiver");
        unregisterReceiver(mConnectionBroadcastReceiver);
        this.finish();
    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        //allows to change modes
        if (swType.isChecked()) {
            showToast("Command Mode");

            Intent intent = new Intent(BGXpressService.ACTION_WRITE_BUS_MODE);
            intent.setClass(this, BGXpressService.class);
            intent.putExtra("busmode", 3);

            startService(intent);

            //send an empty order
            Intent writeIntent = new Intent(BGXpressService.ACTION_WRITE_SERIAL_DATA);
            writeIntent.putExtra("value", "\r\n");
            writeIntent.setClass(getBaseContext(), BGXpressService.class);
            Log.d("INITIALISATION: ", "send an empty order!");

            startService(writeIntent);
        } else {
            Intent intent = new Intent(BGXpressService.ACTION_WRITE_BUS_MODE);
            intent.setClass(this, BGXpressService.class);
            intent.putExtra("busmode", 1);
            startService(intent);

            showToast("Stream Mode");
        }
    }
}
