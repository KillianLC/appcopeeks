package com.example.appcopeeks.Vue;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;

import com.example.appcopeeks.Api.BGX_CONNECTION_STATUS;
import com.example.appcopeeks.Api.BGXpressService;
import com.example.appcopeeks.Controlleur.Capteur;
import com.example.appcopeeks.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class CapteurActivity extends AppCompatActivity {

    private EditText edCapteur;
    private BroadcastReceiver mConnectionBroadcastReceiver;
    static String lastStringReceived = "";
    StringBuffer buffer = new StringBuffer();
    private MainActivity mainActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.capteur_graph_view);
        edCapteur = findViewById(R.id.edCapteur);
        final IntentFilter bgxpressServiceFilter = new IntentFilter(BGXpressService.BGX_CONNECTION_STATUS_CHANGE);
        bgxpressServiceFilter.addAction(BGXpressService.BGX_MODE_STATE_CHANGE);
        bgxpressServiceFilter.addAction(BGXpressService.BGX_DATA_RECEIVED);

        mConnectionBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                switch (intent.getAction()) {
                    //writes the data received in the EditText
                    case BGXpressService.BGX_DATA_RECEIVED: {
                        String stringReceived = intent.getStringExtra("data");

                        if (stringReceived != null) {
                            if (stringReceived.startsWith("[")) {
                                lastStringReceived = buffer.toString();
                                buffer = new StringBuffer();
                            }
                            buffer.append(stringReceived);
                        }
                        Log.i("MES DATA ENTIERE: ", buffer.toString());
                        stringReceived = buffer.toString();

                        //check if the data comes from a sensor
                        if (stringReceived.contains("ID") && stringReceived.contains("Value")) {
                            //sorts the received data using regex
                            Pattern pattern = Pattern.compile(getString(R.string.dataReceived));
                            Matcher matcher = pattern.matcher(stringReceived);

                            while (matcher.find()) {
                                String timestampS = matcher.group(1);
                                String idS = matcher.group(2);
                                String valueS = matcher.group(3);

                                float timestampN = Float.valueOf(timestampS);
                                float idN = Float.valueOf(idS);
                                float valueN = Float.valueOf(valueS);

                                edCapteur.append("Timestamp: " + timestampS + "\r\n");
                                edCapteur.append("Id: " + idS + "\r\n");

                                //lets know which divisor to use
                                int convert;
                                String mCaptDiv = capteurRecordDiv.get(idS);
                                String mCaptName = capteurRecordName.get(idS);
                                String mCaptUnit = capteurRecordUnity.get(idS);
                                try {
                                    if (mCaptDiv.equals("x.3")) {
                                        convert = 1000;
                                        edCapteur.append("Value: " + valueN / convert + "\r\n");
                                        edCapteur.append("Diviseur: " + convert + "\r\n");
                                        edCapteur.append("Name: " + mCaptName + "\r\n");
                                        edCapteur.append("Unity: " + mCaptUnit + "\r\n");
                                        edCapteur.append("_____________" + "\r\n");
                                    }
                                    if (mCaptDiv.equals("x.2")) {
                                        convert = 100;
                                        edCapteur.append("Value: " + valueN / convert + "\r\n");
                                        edCapteur.append("Diviseur: " + convert + "\r\n");
                                        edCapteur.append("Name: " + mCaptName + "\r\n");
                                        edCapteur.append("Unity: " + mCaptUnit + "\r\n");
                                        edCapteur.append("_____________" + "\r\n");
                                    }
                                    if (mCaptDiv.equals("x.1") || mCaptDiv.equals("/10")) {
                                        convert = 10;
                                        edCapteur.append("Value: " + valueN / convert + "\r\n");
                                        edCapteur.append("Diviseur: " + convert + "\r\n");
                                        edCapteur.append("Name: " + mCaptName + "\r\n");
                                        edCapteur.append("Unity: " + mCaptUnit + "\r\n");
                                        edCapteur.append("_____________" + "\r\n");
                                    }
                                } catch (Exception e) {
                                    Log.d("DEBUG", getString(R.string.SensorNotFound));
                                    edCapteur.append("ERROR: " + getString(R.string.SensorNotFound) + "\r\n");
                                    edCapteur.append("_____________" + "\r\n");
                                }
                            }
                        }
                        //Log.i("RECEIVER", buffer.toString());
                    }
                    break;
                    case BGXpressService.BGX_MODE_STATE_CHANGE: {
                        Log.d("bgx_dbg", "BGX Bus Mode Change");
                    }
                    break;
                    case BGXpressService.BGX_CONNECTION_STATUS_CHANGE: {
                        Log.d("bgx_dbg", "BGX Connection State Change");

                        BGX_CONNECTION_STATUS connectionState = (BGX_CONNECTION_STATUS) intent.getSerializableExtra("bgx-connection-status");
                        switch (connectionState) {
                            case CONNECTED:
                                Log.d("bgx_dbg", "DeviceDetails - connection state changed to CONNECTED");
                                break;
                            case CONNECTING:
                                Log.d("bgx_dbg", "DeviceDetails - connection state changed to CONNECTING");
                                break;
                            case DISCONNECTING:
                                Log.d("bgx_dbg", "DeviceDetails - connection state changed to DISCONNECTING");
                                break;
                            case DISCONNECTED:
                                //return to the activity_main if you are not connected
                                Log.d("bgx_dbg", "DeviceDetails - connection state changed to DISCONNECTED");
                                finish();
                                break;
                            case INTERROGATING:
                                Log.d("bgx_dbg", "DeviceDetails - connection state changed to INTERROGATING");
                                break;
                            default:
                                Log.d("bgx_dbg", "DeviceDetails - connection state changed to Unknown connection state.");
                                break;
                        }
                    }
                    break;
                }
            }
        };
        registerReceiver(mConnectionBroadcastReceiver, bgxpressServiceFilter);
        readCapteurData();
    }

    private List<Capteur> capteursSamples = new ArrayList<>();
    //contains the identifier and the divider
    private HashMap<String, String> capteurRecordDiv = new HashMap<>();
    //contains the identifier and the name
    private HashMap<String, String> capteurRecordName = new HashMap<>();
    //contains the identifier and the unit
    private HashMap<String, String> capteurRecordUnity = new HashMap<>();

    public void readCapteurData() {
        InputStream is = getResources().openRawResource(R.raw.capteurs);
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(is, Charset.forName("UTF-8"))
        );
        String line = "";
        try {
            while ((line = reader.readLine()) != null) {
                //split by ";"
                String[] tokens = line.split(";");
                //Read the data
                Capteur sample = new Capteur("", "", "", "", "", "", "", "", "", "");
                sample.setId(tokens[0]);
                sample.setType(tokens[1]);
                sample.setUnity(tokens[2]);
                sample.setInput_format(tokens[3]);
                sample.setOut_put(tokens[4]);
                sample.setFull_name(tokens[5]);
                sample.setIcon(tokens[6]);
                sample.setMax(tokens[7]);
                sample.setMin(tokens[8]);
                sample.setFormat(tokens[9]);

                capteurRecordName.put(sample.getId(), sample.getFull_name());
                capteurRecordUnity.put(sample.getId(), sample.getUnity());
                capteurRecordDiv.put(sample.getId(), sample.getInput_format());

                capteursSamples.add(sample);
            }
            //Log.d("MyActivty"," : "+capteursSamples);
            Log.d("HashMap", "MAP : " + capteurRecordDiv);
        } catch (IOException e) {
            Log.i("DEBUG", "Error reading data file on line " + line, e);
            e.printStackTrace();
        }
    }

    protected void onResume() {
        super.onResume();

    }

    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mConnectionBroadcastReceiver);
        this.finish();
    }
}
