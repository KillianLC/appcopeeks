package com.example.appcopeeks.Vue;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import com.example.appcopeeks.Api.BGX_CONNECTION_STATUS;
import com.example.appcopeeks.Api.BGXpressService;
import com.example.appcopeeks.R;

import static com.example.appcopeeks.Api.BGXpressService.BGX_CONNECTION_ERROR;


public class IndeterminateProgressActivity extends AppCompatActivity {

    BroadcastReceiver mBroadcastReceiver;

    Button mCancelButton;
    TextView mStatusLabel;
    Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_indeterminate_progress);

        final IndeterminateProgressActivity myActivity = this;

        mCancelButton = findViewById(R.id.cancelButton);

        mStatusLabel = findViewById(R.id.connectionStatusTextView);
        mStatusLabel.setText("CONNECTING");

        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("bgx_dbg", "cancel button clicked.");
                BGXpressService.startActionBGXCancelConnect(myActivity);
                myActivity.finish();
            }
        });

        mHandler = new Handler();

        final IntentFilter bgxpressServiceFilter = new IntentFilter(BGXpressService.BGX_CONNECTION_STATUS_CHANGE);
        bgxpressServiceFilter.addAction(BGX_CONNECTION_ERROR);

        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                getStatus(context, intent);
            }
        };
        registerReceiver(mBroadcastReceiver, bgxpressServiceFilter);
    }

    //get the status of the connection
    public void getStatus(Context context, Intent intent) {
        switch (intent.getAction()) {
            case BGX_CONNECTION_ERROR:
                switch (intent.getIntExtra("Status", -1)) {
                    case 137: ///< GATT_AUTH_FAIL
                        mStatusLabel.setText("BONDED FAIL");
                        break;
                    default:
                        mStatusLabel.setText("ERROR");
                        break;
                }
                break;
            case BGXpressService.BGX_CONNECTION_STATUS_CHANGE: {
                BGX_CONNECTION_STATUS stateValue = (BGX_CONNECTION_STATUS) intent.getSerializableExtra("bgx-connection-status");
                Log.d("bgx_dbg", "BGX Connection State Change: " + stateValue);
                if (BGX_CONNECTION_STATUS.CONNECTING == stateValue) {
                    mStatusLabel.setText("CONNECTING");
                } else if (BGX_CONNECTION_STATUS.INTERROGATING == stateValue) {
                    mStatusLabel.setText("INTERROGATING");
                } else if (BGX_CONNECTION_STATUS.CONNECTED == stateValue) {
                    mStatusLabel.setText("CONNECTED");
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            finish();
                        }
                    }, 500);

                } else if (BGX_CONNECTION_STATUS.DISCONNECTED == stateValue) {
                    finish();
                }
            }
            break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mBroadcastReceiver);
    }
}


