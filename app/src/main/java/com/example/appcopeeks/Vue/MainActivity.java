package com.example.appcopeeks.Vue;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.appcopeeks.Adapter.BGXDeviceListAdapter;
import com.example.appcopeeks.Api.BGX_CONNECTION_STATUS;
import com.example.appcopeeks.Api.BGXpressService;
import com.example.appcopeeks.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import static com.example.appcopeeks.Api.BGX_CONNECTION_STATUS.CONNECTED;

public class MainActivity extends AppCompatActivity {

    protected TextView tvDiscance;
    protected Handler mHandler;
    protected BroadcastReceiver mDeviceDiscoveryReceiver;
    protected ArrayList<Map<String, String>> mScanResults;
    protected RecyclerView.Adapter mDeviceListAdapter;
    protected RecyclerView mDeviceListRecyclerView;
    protected RecyclerView.LayoutManager mDeviceListLayoutManager;
    protected ImageView imgAbout;
    protected static final long SCAN_PERIOD = 10000;
    protected boolean fLocationPermissionGranted = false;
    protected static final int PERMISSION_REQUEST_COARSE_LOCATION = 456;
    protected String leftRssi;
    protected String rightRssi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imgAbout = findViewById(R.id.imgAbout);
        tvDiscance = findViewById(R.id.tvDistance);
        Log.d("DEBUG", "1");

        //check if the device has Bluetooth
        getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE);
        this.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
        startService(new Intent(this, BGXpressService.class));

        mDeviceDiscoveryReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (intent.getAction()) {

                    //adding devices in HashMap
                    case BGXpressService.BGX_SCAN_DEVICE_DISCOVERED: {
                        HashMap<String, String> deviceRecord = (HashMap<String, String>) intent.getSerializableExtra("DeviceRecord");
                        mScanResults.add(deviceRecord);

                        Log.d("DEBUG", "SIZE " + " " + mScanResults.size());

                        //show the HashMap in the logs
                        for (int i = 0; i < mScanResults.size(); i++) {
                            Log.d("DEBUG", "" + mScanResults.get(i));
                        }
                        Collections.sort(mScanResults, new Comparator<Map<String, String>>() {
                            @Override
                            public int compare(Map<String, String> leftRecord, Map<String, String> rightRecord) {
                                leftRssi = leftRecord.get("rssi");
                                rightRssi = rightRecord.get("rssi");

                                Log.d("RSSI:", leftRssi+" "+rightRssi);

                                int result = leftRssi.compareTo(rightRssi);
                                Log.d("DISTANCE",Integer.toString(result));

                                return leftRssi.compareTo(rightRssi);
                            }
                        });

                        mDeviceListAdapter = new BGXDeviceListAdapter(getApplicationContext(), mScanResults);
                        mDeviceListRecyclerView.swapAdapter(mDeviceListAdapter, true);
                    }
                    break;
                    //if the device is Connected, launch IndeterminateProgressActivity
                    case BGXpressService.BGX_CONNECTION_STATUS_CHANGE: {
                        BGX_CONNECTION_STATUS connectionStatusValue = (BGX_CONNECTION_STATUS) intent.getSerializableExtra("bgx-connection-status");
                        if (CONNECTED == connectionStatusValue) {
                            BluetoothDevice btDevice = intent.getParcelableExtra("device");
                            Intent intent2 = new Intent(context, ServiceActivity.class);
                            intent2.putExtra("BLUETOOTH_DEVICE", btDevice);
                            intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent2);
                        }
                    }
                    break;
                }
            }
        };

        //for more details look at the line 936 in (BGXpressService)
        IntentFilter listIntentFilter = new IntentFilter(BGXpressService.BGX_SCAN_DEVICE_DISCOVERED);
        listIntentFilter.addAction(BGXpressService.BGX_CONNECTION_STATUS_CHANGE);
        listIntentFilter.addAction(BGXpressService.BGX_SCAN_MODE_CHANGE);

        registerReceiver(mDeviceDiscoveryReceiver, listIntentFilter);

        mDeviceListRecyclerView = findViewById(R.id.rvDevice);

        //block the size of the view
        mDeviceListRecyclerView.setHasFixedSize(true);

        mDeviceListLayoutManager = new LinearLayoutManager(this);
        mDeviceListRecyclerView.setLayoutManager(mDeviceListLayoutManager);

        if (null == mScanResults) {
            //avoid crash if the HashMap is empty
            mScanResults = new ArrayList<>();
        }

        mDeviceListAdapter = new BGXDeviceListAdapter(this, mScanResults);
        mDeviceListRecyclerView.setAdapter(mDeviceListAdapter);
        mHandler = new Handler();

    }

    //check permissions
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission granted, yay! Start the Bluetooth device scan.
                    Log.d("bgx_dbg", "Received permissions to use location.");
                    fLocationPermissionGranted = true;
                    Log.d("bgx_dbg", "starting scan");
                    BGXpressService.startActionStartScan(this);
                } else {
                    // Alert the user that this application requires the location permission to perform the scan.
                    Log.e("bgx_dbg", "Did not get permissions to use location.");
                    fLocationPermissionGranted = false;
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mDeviceDiscoveryReceiver);
        Log.i("INfO: ", "Liste des appareils supprimé");
    }

    //Enable bluetooth
    @Override
    protected void onResume() {

        super.onResume();
        boolean fAdapterEnabled = BluetoothAdapter.getDefaultAdapter().isEnabled();
        try {
            //enable Bluetooth
            if (!fAdapterEnabled) {
                Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivity(intent);
            }
            //check if the device has the Bluetooth
            if (fAdapterEnabled && fLocationPermissionGranted) {
                if (null == BluetoothAdapter.getDefaultAdapter()) {
                    Log.d("bgx_dbg", "bluetooth adapter is not available.");
                } else {
                    scanForDevices();
                }
            }
        } catch (Exception e) {
            Log.d("bgx_dbg", "Exception caught while calling isEnabled.");
            Toast.makeText(this, "Exception caught", Toast.LENGTH_LONG).show();
        }
    }
    ///////////////METHODES/////////////////

    //start the scan if the device has Bluetooth
    protected void scanForDevices() {
        final Context myContext = this;

        mScanResults.clear();
        mDeviceListAdapter = new BGXDeviceListAdapter(getApplicationContext(), mScanResults);
        mDeviceListRecyclerView.swapAdapter(mDeviceListAdapter, true);

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d("bgx_dbg", "starting scan");

                BGXpressService.startActionStartScan(myContext);
            }
        }, 0);

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d("bgx_dbg", "stopping scan");
                BGXpressService.startActionStopScan(myContext);

            }
        }, SCAN_PERIOD);
    }

    public void aboutPage(View view) {
        Intent intent = new Intent(MainActivity.this, AboutActivity.class);
        startActivity(intent);
    }

    public void imgRefresh(View view) {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }
}
